package ictgradschool.industry.lab14.ex03;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class CancellablePrimeFactorsSwingApp extends JPanel {

    private JButton _startBtn;// Button to start the calculation process.
    private JButton _stopBtn; // Button to stop the calculation process.
    private JTextArea _factorValues;  // Component to display the result.
    PrimeFactorisationWorker pfw;

    private class PrimeFactorisationWorker extends SwingWorker<List<Long>, Void> {
        // SwingWorker< T, V > <-- this is the structure

        private long n;

        private PrimeFactorisationWorker(long n) {
            this.n = n;
        }

        @Override
        protected List doInBackground() throws Exception {

            while (!isCancelled()) {
                List<Long> myPrimeList = new ArrayList<>();

                for (long i = 2; i * i <= n; i++) {

                    // If i is a factor of N, repeatedly divide it out
                    while (n % i == 0) {
//                    _factorValues.append(i + "\n");
                        myPrimeList.add(i);

                        n = n / i;
                    }
                }

                // if biggest factor occurs only once, n > 1
                if (n > 1) {
                    myPrimeList.add(n);
                }

                // confirm protected "List" should be there
                // Needs to return a list
                // this will be executed inside a seperate thread

                return myPrimeList;
            }
            return null;
        }


        @Override
        public void done() {
            _startBtn.setEnabled(true);
            _stopBtn.setEnabled(false);
            try {
                if (!isCancelled()) {
                    List<Long> resultList = get();
                    String settingText = "";
                    for (int i = 0; i < resultList.size(); i++) {
                        settingText += resultList.get(i) + " ";
                    }
                    _factorValues.setText(settingText);
                }

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

        }
    }

    public CancellablePrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);

        _startBtn = new JButton("start");
        _stopBtn = new JButton("stop");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);


        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);
                _stopBtn.setEnabled(true);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                // Start the computation in the Event Dispatch thread.


                // EXECUTE FUNCTION
                pfw = new PrimeFactorisationWorker(n);
                pfw.execute();

                // Re-enable the Start button.
//                _startBtn.setEnabled(true);

                // Restore the cursor.
                setCursor(Cursor.getDefaultCursor());
            }
        });

        _stopBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

//                while (!_startBtn.isEnabled()) {
//                _startBtn.setEnabled(false);

                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                while (!_startBtn.isEnabled()) {
                    pfw.cancel(true);
                    pfw = null;
                }


                setCursor(Cursor.getDefaultCursor());
//                }
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_stopBtn);
        _stopBtn.setEnabled(false);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new CancellablePrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();

            }
        });
    }
}



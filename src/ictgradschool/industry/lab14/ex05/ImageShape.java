package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;
    private loadImages backgroundImage;
    private URL url;


    //URI
    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    //URL
    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    //url
    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);


        this.url = url;

        //EXECUTE STATEMENT
        backgroundImage = new loadImages();
        backgroundImage.execute();


    }

    @Override
    public void paint(Painter painter) {


        if (image == null) {

            painter.drawRect(fX, fY, fWidth, fHeight);
            painter.drawCenteredText("Loading...",fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);

        } else painter.drawImage(this.image, fX, fY, fWidth, fHeight);

    }


    private class loadImages extends SwingWorker<Image, Void> {

        @Override
        protected Image doInBackground() throws Exception {

            Image image = ImageIO.read(url);
            if (fWidth == image.getWidth(null) && fHeight == image.getHeight(null)) {
                return image;
            } else {

                Image i = image.getScaledInstance(fWidth, fHeight, Image.SCALE_SMOOTH);
                i.getWidth(null);
                return i;
            }

        }


        @Override
        public void done() {
            try {

                image = get();


            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();

            }
        }

    }

}
